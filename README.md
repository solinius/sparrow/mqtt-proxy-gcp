# MQTT Proxy - GCP IoT Core

## Description
This is a standalone microservice that acts as a proxy between non-TLS MQTT clients and GCP IoT Core, which requires TLS connections.

## Prerequisites
* Google Cloud Platform IoT Core device registry

## Quick Start
`docker pull registry.gitlab.com/solinius/sparrow/mqtt-proxy-gcp:latest`

`docker run mqtt-proxy-gcp -e GCP_PROJECT_ID=my_project -e PRIVATE_KEY_FILE=/path/to/private_key.pem -d`