require('dotenv').config();

let mosca = require('mosca');
let iot = require('./iot');

let deviceMap = [];

let pubSubSettings = {
    type: 'mongo',
    url: 'mongodb://mongo_mqtt:27017/mqtt',
    pubsubCollection: 'pubsub',
    mongo: {}
};

let settings = {
    port: 1883,
    backend: pubSubSettings
};

let server = new mosca.Server(settings);

// fired when the mqtt server is ready
server.on('ready', function() {
    console.log('Mosca server is up and running');
});

server.on('clientConnected', function(client) {
    // client id should be something like:
    // projects/{project-id}/locations/{cloud-region}/registries/{registry-id}/devices/{device-id}
    if (client.id.startsWith('relay-')) {
        return;
    }

    let deviceId = getDeviceId(client.id);
    log(deviceId, 'connected');

    // connect to IoT Core with the client's ID topic and keep track of the connection
    let device = new iot.IoTDevice(client.id);
    device.connect();
    deviceMap[client.id] = device;
});

// when client subscribes to a topic
server.on('subscribed', function(topic, client) {
    if (topic.startsWith('$SYS') || client.id.startsWith('relay-')) {
        return;
    }

    let deviceId = getDeviceId(client.id);

    // the device subscribed to its config topic
    log(deviceId, 'subscribed to config');

    // subscribe to the topic in pubsub
    let device = deviceMap[client.id];
    device.subscribeToConfig();
});

// when a PUBLISH message is received from a client
server.on('published', function(packet, client) {
    if (packet.topic.startsWith('$SYS') || client.id.startsWith('relay-')) {
        return;
    }

    let deviceId = getDeviceId(client.id);

    log(deviceId, 'sent PUBLISH ' + packet.topic);

    // forward this message along to Cloud IoT
    let device = deviceMap[client.id];
    device.publish(packet.topic, packet.payload);
});

// when client unsubscribes from a topic
server.on('unsubscribed', function(topic, client) {
    let deviceId = getDeviceId(client.id);
    log(deviceId, 'unsubscribed');

    // unsubscribe from the topic in pubsub
    let device = deviceMap[client.id];
    device.unsubscribeFromConfig();
});

// when client sends puback for a published message
server.on('delivered', function(packet, client) {
    let deviceId = getDeviceId(client.id);
    log(deviceId, 'sent PUBACK');
});

server.on('clientDisconnected', function(client) {
    if (client.id.startsWith('relay-')) {
        return;
    }

    let deviceId = getDeviceId(client.id);
    log(deviceId, 'disconnected');

    // disconnect this client's IoT Core connection
    let device = deviceMap[client.id];
    device.disconnect();
});

server.on('clientDisconnecting', function(client) {
    let deviceId = getDeviceId(client.id);
    log(deviceId, 'disconnecting');
});

function getDeviceId(clientId) {
    let parts = clientId.split('/');
    return parts[parts.length - 1];
}

function log(deviceId, message) {
    let now = new Date();
    console.log(now.toISOString() + ': ' + deviceId + ' ' + message);
}