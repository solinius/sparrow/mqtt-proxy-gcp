const mqtt = require('mqtt');
const jwt = require('jsonwebtoken');
const fs = require('fs');

const projectId = process.env.GCP_PROJECT_ID;
const privateKeyFile = process.env.PRIVATE_KEY_FILE;
const privateKey = fs.readFileSync(privateKeyFile);

function createJwt() {
    const token = {
        'iat': Date.now() / 1000,
        'exp': (Date.now() / 1000) + 20 * 60, // 20 minutes
        'aud': projectId
    };
    return jwt.sign(token, privateKey, { algorithm: 'ES256' });
}

class IoTDevice {

    constructor(mqttClientId) {
        this.clientId = mqttClientId;
        let parts = mqttClientId.split('/');
        this.deviceId = parts[parts.length - 1];
        this.configTopic = '/devices/' + this.deviceId + '/config';
    }

    connect() {
        /**
         * The IoT client subscribes to the Cloud IoT config topic, and receives a message when the config is updated.
         * It publishes messages to the IoT Core telemetry topic when devices send messages.
         * @type {MqttClient}
         */
        this.iotClient = mqtt.connect({
            host: 'mqtt.googleapis.com',
            port: 8883,
            clientId: this.clientId,
            username: 'unused',
            password: createJwt(),
            protocol: 'mqtts',
            secureProtocol: 'TLSv1_2_method'
        });

        this.iotClient.on('error', IoTDevice.handleMqttError);

        // todo: if there's already a subscriber with this relay-deviceId (broken earlier connection, etc), destroy it first

        /**
         * The relay client publishes messages to the Mosca broker's topic that the end device is listening to.
         * @type {MqttClient}
         */
        this.relayClient = mqtt.connect({
            host: 'localhost',
            port: 1883,
            clientId: 'relay-' + this.deviceId,
            username: '',
            password: '',
            protocol: 'mqtt'
        });

        this.relayClient.on('error', IoTDevice.handleMqttError);
    }

    disconnect() {
        this.iotClient.end();
        this.relayClient.end();
    }

    publish(topic, message) {
        this.iotClient.publish(topic, message);
    }

    /**
     * Subscribe to the device's config topic and set up internal callback for when messages are received.
     */
    subscribeToConfig() {
        this.iotClient.subscribe(this.configTopic);
        this.iotClient.on('message', (topic, message, packet) => {
            // when a message is received from IoT core, publish it to the Mosca topic the device is listening to
            this.relayClient.publish(this.configTopic, message);
            log(this.deviceId, "relayed config from GCP");
        });
    }

    unsubscribeFromConfig() {
        this.iotClient.unsubscribe(this.configTopic);
    }

    static handleMqttError(err) {
        console.log(err);
    }
}

function log(deviceId, message) {
    let now = new Date();
    console.log(now.toISOString() + ': ' + deviceId + ' ' + message);
}

module.exports.IoTDevice = IoTDevice;